��    r      �  �   <      �	  2   �	  E   �	     *
     2
     E
     V
     Z
     r
     ~
     �
  	   �
     �
  
   �
     �
     �
     �
  (   �
     �
     �
     �
               .  	   6  
   @  
   K     V  	   v     �     �     �     �  
   �     �     �     �     �                         *  	   /     9     @     G  T   O     �     �     �     �  �  �     ]     e     m     �     �     �     �     �     �  P   �     "     /     8     @     I     V     [     b     g     �     �     �     �  	   �     �     �     �  
   �     �     �     �       �     )   �     �     �  }   �     u  L   �     �     �  >   �          7     F     ^     f  #   y     �  �   �     A  R   ]     �     �     �     �     �     �     �     �     �  <  �  >   0  G   o     �     �     �  
   �  	   �  
   �  	   �       
     
         +  	   >     H     _  '   g     �     �     �     �     �     �     �     �  
   �  (   �               &     ?     U     ]     j     w     �     �     �  	   �     �  
   �     �  	   �     �     �     �  X   �     D     L     R     f  �  n               "     4     F     \     k     t     �  V   �     �     �     �     �                    #  %   *     P     W     f     �  	   �     �     �  
   �     �     �     �     �     �  �   �  #   }     �  #   �  v   �     R  b   _     �     �  F   �          0     ?     \     d     |     �  �   �     2  T   K     �     �     �     �     �     �     �     �     �     e   -   m   T   h       3              Z              X   \           V   &          9       !   H   K              k               ^   B   =   6       D   f   4   .           W   ,   $   r       8                 (   0   d       F   `              U   I   >   q   1      /                 "   @           j   Y          %   l   #   A   ;   S   ?      g       R   2   '   _           	   5          c          P      ]   b   a         p   i   C   [       E                 J   7                 +                     )   *   :   
   N       <   L   n           G   M      Q   o      O             

Your settings have been reset to default values. . Time must be added to the account for continued use of the service. Account Account management Account number:  Add Add time to the account Address: %s Advanced Any Australia Austria Auto start Bad Bad account number. Belgium Block the internet on connection failure Canada Cancel Change account number Close Configuration file Connect Connected Connecting Connection Connection failed.

Details:
%s Country:  Country: %s Create a new trial account Current version: %s Denmark Disconnect Disconnected Enter the account number Error Error report Finland France Germany Get started Help Hong Kong IPv4:  IPv6:  Iceland It seems that Mullvad is already running.
Only one instance of Mullvad is allowed.

 Italy Japan Latest version: %s Manage Manage which public ports will be forwarded to
your computer through the VPN tunnel.
Only use this if you have a network service on
your computer that needs to be accessible through
the exit IP of the server you connect to.

Example:
If you get port 1234 forwarded and are connected
to server se5.mullvad.net, then you will be
reachable on se5x.mullvad.net:1234

Changes take effect after reconnecting. Morocco Mullvad Mullvad: connected Mullvad: connecting Mullvad: disconnected Netherlands Network New Zealand Norway Obfsproxy is not installed.

You can install it with:
apt-get install obfsproxy
 Port manager Port: %s Ports:  Portugal Protocol: %s Quit Remove Send Sending failed.

Details:
 Server Settings Settings for Mullvad Shutting down Singapore South Korea Spain Status Status: %s Stop DNS leaks Sweden Switzerland Taiwan The TAP-Windows virtual network adapter is missing.

Restore it by running the installation program again. (Quit the program first.) The account has expired.

Manage account? The trial period expires in  This information will be sent: This version of Mullvad does not support Windows XP. Please upgrade your operating system, or use a plain OpenVPN connection. Time left: %s To facilitate problem solving you can send debug information to mullvad.net. Tunnel IPv6 USA Unable to create a new account
Check your internet connection. Unable to fetch address United Kingdom Use an existing account Version Welcome to Mullvad Write a description of the problem: Your email address: Your internet traffic is now encrypted 
and anonymized using the Mullvad servers 
in order to deny third parties access to 
your communication. Your new account number: {} Your settings.ini file was corrupt.
A backup of your settings have been saved to:
 day days error %s fetching ... hour hours none none [port] unknown Project-Id-Version: Mullvad
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-30 13:59+0200
PO-Revision-Date: 2015-09-14 14:32+0200
Last-Translator: Simon Andersson <simon@mullvad.net>
Language-Team: Swedish
Language: Swedish
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 

Dina inställningar har återställts till ursprungsvärden. .
För att fortsätta använda tjänsten
måste kontotiden förlängas. Konto Hantera kontot Kontonummer:  Lägg till Förläng Adress: %s Avancerat Vilket som helst Australien Österrike Starta automatiskt Felaktigt Felaktigt kontonummer. Belgien Blockera internet om anslutningen bryts Kanada Avbryt Ändra kontonummer Stäng Konfigurationsfil Anslut Ansluten Ansluter Anslutning Anslutningen misslyckades.

Detaljer:
%s Land:  Land: %s Skapa ett nytt provkonto Nuvarande version: %s Danmark Koppla från Frånkopplad Ange kontonummer fel Rapportera ett fel Finland Frankrike Tyskland Kom igång Hjälp Hong Kong IPv4:  IPv6:  Island Det verkar som att Mullvad redan är igång.
Bara en instans av Mullvad är tillåtet.

 Italien Japan Senaste version: %s Hantera Administrera vilka publika portar som skall
vidarebefordras till din dator genom VPN-tunneln.
Använd endast detta om du har en nätverkstjänst på
din dator som behöver vara nåbar via den IP som din
VPN-trafik går ut genom.

Exempel:
Om du får port 1234 vidarebefordrad och är ansluten
till se5.mullvad.net så kommer din dator vara nåbar
på se5x.mullvad.net:1234

Ändringar får verkan efter återanslutning. Marocko Mullvad Mullvad: ansluten Mullvad: ansluter Mullvad: frånkopplad Nederländerna Nätverk Nya Zeeland Norge Obfsproxy är inte installerat.

Du kan installera det med:
apt-get install obfsproxy
 Porthanterare Port: %s Portar: Portugal Protokoll: %s Avsluta Ta bort Skicka Sändningen misslyckades.

Detaljer:
 Server Inställningar Inställningar för Mullvad Avslutar Singapore Sydkorea Spanien Tillstånd Tillstånd: %s Förhindra DNS-läckor Sverige Schweiz Taiwan Den virtuella nätverksenheten TAP-Windows saknas.

Återställ den genom att köra installationsprogrammet igen. (Avsluta programmet först.) Tiden har gått ut.

Hantera konto? Provtiden går ut om  Information som kommer att skickas: Den här versionen av Mullvad stödjer inte Windows XP. Var god uppgradera ditt operativsystem, eller använd OpenVPN. Tid kvar: %s För att underlätta lösningen av problem kan du skicka felsökningsinformation till mullvad.net. Tunnla IPv6 USA Kunde ej skapa nytt konto
Vänligen kontrollera din internetanslutning Kunde ej hämta adress Storbritannien Använd ett befintligt konto Version Välkommen till Mullvad Beskriv problemet: Din epostadress: Nu krypteras och anonymiseras 
internettrafiken med hjälp av Mullvads 
servrar för att inte utomstående ska
kunna ta del av din kommunikation. Ditt nya kontonummer: {} Din settings.ini-fil var korrupt.
En kopia av dina inställningar har sparats till:
 dag dagar fel %s hämtar ... timme timmar inget ingen okänt 