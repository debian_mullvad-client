��    p      �  �         p	  2   q	  E   �	     �	     �	     
     
     
     2
     >
     G
  	   K
     U
  
   ]
     h
     l
     �
  (   �
     �
     �
     �
     �
     �
     �
  	   �
  
      
          	   6     @     L     g     {  
   �     �     �     �     �     �     �     �     �  	   �     �     �       T   	     ^     d     j     }     �     �     �     �     �     �     �     �     �  P   �     I     V     _     g     p     }     �     �     �     �     �     �     �  	   �     �     �     �  
   �     
                ,  �   3  )   �     �     �  }        �  L   �     �       >        F     ^     m     �     �  #   �     �  �   �     h  R   �     �     �     �     �     �     �                 <    2   W  E   �     �     �     �     �                $     -  	   1     ;  
   C     N     R     f  (   n     �     �     �     �     �     �  	   �  
   �  
   �     �  	        &     2     M     a  
   i     t     �     �     �     �     �     �     �  	   �     �     �     �  T   �     H     N     T     g     n     v     ~     �     �     �     �     �     �  P   �     3     @     I     P     Y     f     k     r     w     �     �     �     �  	   �     �     �     �  
   �     �          	       �     )   �     �     �  }        �  L   �     �     �  >   �     /     G     V     n     v  #   �     �  �   �     Q  R   m     �     �     �     �     �     �     �     �     �     ^   $   :      Y   G      W   '   _       L      n   .   @   <   2           0   )       +   ?   B   O   1      9   J       3   #          >                    ;              [   l   U   X   T           M          F   \   I      k   i   N          %   4   b      ]          d   !              `               H          -   ,   8                      "   h             Z          E   V      g   C   m   P   c      K   6       7   *          j   	   a   Q   
   /       R   A             D              &   p         e               S   (   =           5   o   f    

Your settings have been reset to default values. . Time must be added to the account for continued use of the service. Account Account management Account number:  Add Add time to the account Address: %s Advanced Any Australia Austria Auto start Bad Bad account number. Belgium Block the internet on connection failure Canada Cancel Change account number Close Configuration file Connect Connected Connecting Connection Connection failed.

Details:
%s Country:  Country: %s Create a new trial account Current version: %s Denmark Disconnect Disconnected Enter the account number Error report Finland France Germany Get started Help Hong Kong IPv4:  IPv6:  Iceland It seems that Mullvad is already running.
Only one instance of Mullvad is allowed.

 Italy Japan Latest version: %s Manage Morocco Mullvad Mullvad: connected Mullvad: connecting Mullvad: disconnected Netherlands Network New Zealand Norway Obfsproxy is not installed.

You can install it with:
apt-get install obfsproxy
 Port manager Port: %s Ports:  Portugal Protocol: %s Quit Remove Send Sending failed.

Details:
 Server Settings Settings for Mullvad Shutting down Singapore South Korea Spain Status Status: %s Stop DNS leaks Sweden Switzerland Taiwan The TAP-Windows virtual network adapter is missing.

Restore it by running the installation program again. (Quit the program first.) The account has expired.

Manage account? The trial period expires in  This information will be sent: This version of Mullvad does not support Windows XP. Please upgrade your operating system, or use a plain OpenVPN connection. Time left: %s To facilitate problem solving you can send debug information to mullvad.net. Tunnel IPv6 USA Unable to create a new account
Check your internet connection. Unable to fetch address United Kingdom Use an existing account Version Welcome to Mullvad Write a description of the problem: Your email address: Your internet traffic is now encrypted 
and anonymized using the Mullvad servers 
in order to deny third parties access to 
your communication. Your new account number: {} Your settings.ini file was corrupt.
A backup of your settings have been saved to:
 day days error %s fetching ... hour hours none none [port] unknown Project-Id-Version: Mullvad
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-30 13:59+0200
PO-Revision-Date: 2015-09-14 14:40+0200
Last-Translator: Simon Andersson <simon@mullvad.net>
Language-Team: English
Language: English
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 

Your settings have been reset to default values. .
Time must be added to the account
for continued use of the service. Account Account management Account number:  Add Add time to the account Address: %s Advanced Any Australia Austria Auto start Bad Bad account number. Belgium Block the internet on connection failure Canada Cancel Change account number Close Configuration file Connect Connected Connecting Connection Connection failed.

Details:
%s Country:  Country: %s Create a new trial account Current version: %s Denmark Disconnect Disconnected Enter the account number Report a problem Finland France Germany Get started Help Hong Kong IPv4:  IPv6:  Iceland It seems that Mullvad is already running.
Only one instance of Mullvad is allowed.

 Italy Japan Latest version: %s Manage Morocco Mullvad Mullvad: connected Mullvad: connecting Mullvad: disconnected Netherlands Network New Zealand Norway Obfsproxy is not installed.

You can install it with:
apt-get install obfsproxy
 Port manager Port: %s Ports: Portugal Protocol: %s Quit Remove Send Sending failed.

Details:
 Server Settings Settings for Mullvad Shutting down Singapore South Korea Spain Status Status: %s Stop DNS leaks Sweden Switzerland Taiwan The TAP-Windows virtual network adapter is missing.

Restore it by running the installation program again. (Quit the program first.) The account has expired.

Manage account? The trial period expires in  This information will be sent: This version of Mullvad does not support Windows XP. Please upgrade your operating system, or use a plain OpenVPN connection. Time left: %s To facilitate problem solving you can send debug information to mullvad.net. Tunnel IPv6 USA Unable to create a new account
Check your internet connection. Unable to fetch address United Kingdom Use an existing account Version Welcome to Mullvad Write a description of the problem: Your email address: Your internet traffic is now encrypted 
and anonymized using the Mullvad servers 
in order to deny third parties access to 
your communication. Your new account number: {} Your settings.ini file was corrupt.
A backup of your settings have been saved to:
 day days error %s fetching ... hour hours none none unknown 