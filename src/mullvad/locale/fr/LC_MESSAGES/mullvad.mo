��    `        �         (  2   )  E   \     �     �     �     �     �     �     �     �  
   	     	     	  (   &	     O	     V	     ]	     s	     y	     �	  	   �	  
   �	  
   �	     �	  	   �	     �	     �	     
  
   
     $
     1
     J
     W
     _
     k
     p
     w
     ~
  T   �
     �
     �
  �  �
     �     �     �     �     �     �  P   �     2     ?     H     P     ]     b     i     n     �     �     �     �  	   �     �  
   �     �     �  �   �  )   s     �     �  }   �     W  L   e     �     �  >   �               1     9  #   L  �   p       �   #     �  R   �     "     &     +     4     A     F     L     Q     ]    e  F   h  T   �                    1     9     X     e     m     r     �     �  1   �     �     �     �            	   ,  	   6  	   @  	   J  &   T     {     �      �     �     �     �     �     �  	     	             !     '     ,  k   4     �     �    �     �     �     �     �            X        w     �     �     �     �  	   �     �     �     �     �     �       	   #     -  
   4     ?     X  �   _  -          .  "   O  �   r       f        �     �  T   �  #   �          '     /  &   E  �   l     !  �   7  #   �  e   �     V     [  	   a     k     }     �     �     �     �         9   /   X           G   :      5                   Q   S   J   `             >             A   R   ?             2   +   H          <   Z   #   V           1      &   P   .      -      6       _          "   %       0           8   T   *   I       C      (   E   )                       @            N   O      3   $   B   ,   ]           D              \   L   	       =   ^   [      
   K   W                      4                        !       F   Y       M   U   ;       '      7    

Your settings have been reset to default values. . Time must be added to the account for continued use of the service. Account Account management Account number:  Add Add time to the account Address: %s Advanced Any Auto start Bad Bad account number. Block the internet on connection failure Canada Cancel Change account number Close Configuration file Connect Connected Connecting Connection Connection failed.

Details:
%s Country:  Country: %s Create a new trial account Current version: %s Disconnect Disconnected Enter the account number Error report Germany Get started Help IPv4:  IPv6:  Iceland It seems that Mullvad is already running.
Only one instance of Mullvad is allowed.

 Latest version: %s Manage Manage which public ports will be forwarded to
your computer through the VPN tunnel.
Only use this if you have a network service on
your computer that needs to be accessible through
the exit IP of the server you connect to.

Example:
If you get port 1234 forwarded and are connected
to server se5.mullvad.net, then you will be
reachable on se5x.mullvad.net:1234

Changes take effect after reconnecting. Mullvad Mullvad: connected Mullvad: connecting Mullvad: disconnected Netherlands Network Obfsproxy is not installed.

You can install it with:
apt-get install obfsproxy
 Port manager Port: %s Ports:  Protocol: %s Quit Remove Send Sending failed.

Details:
 Server Settings Settings for Mullvad Shutting down Singapore Status Status: %s Stop DNS leaks Sweden The TAP-Windows virtual network adapter is missing.

Restore it by running the installation program again. (Quit the program first.) The account has expired.

Manage account? The trial period expires in  This information will be sent: This version of Mullvad does not support Windows XP. Please upgrade your operating system, or use a plain OpenVPN connection. Time left: %s To facilitate problem solving you can send debug information to mullvad.net. Tunnel IPv6 USA Unable to create a new account
Check your internet connection. Unable to fetch address Use an existing account Version Welcome to Mullvad Write a description of the problem: You are running Mullvad from the install image.
This is not supported, you have to first copy the program to the Applications directory and run it from there. Your email address: Your internet traffic is now encrypted 
and anonymized using the Mullvad servers 
in order to deny third parties access to 
your communication. Your new account number: {} Your settings.ini file was corrupt.
A backup of your settings have been saved to:
 day days error %s fetching ... hour hours none none [port] unknown Project-Id-Version: Mullvad
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-30 13:59+0200
PO-Revision-Date: 
Last-Translator: 
Language-Team: French
Language: French
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 

Votre configuration a été réinitialisée aux valeurs par défaut. .
Vous devez ajouter du temps à votre compte pour continuer à utiliser ce service. Compte Gestion du compte Numéro de compte:  Ajouter Ajouter du temps sur le compte Addresse: %s Avancé Tout Démarrage automatique Mauvais Numéro de compte incorrect. Bloquer Internet lors de l'échec de la connexion Canada Annuler Changer le numéro de compte Fermer Fichier de configuration Connecter Connecté Connexion Connexion La connexion a échoué.

Détails:
%s Pays:  Pays: %s Créer un nouveau compte d'essai Version actuelle: %s Déconnecter Déconnecté Entrer le numéro de compte Rapport d'erreur Allemagne Commencer Aide IPv4: IPv6 Islande Il semblerait que Mullvad est déjà en cours d'exécution.
Une seule instance de Mullvad est autorisée.

 Dernière version: %s Gérer Configurer les ports publics qui seront relayés vers 
votre ordinateur à travers le tunnel VPN.
Utiliser uniquement cette configuration si vous avez sur votre 
ordinateur un service réseau qui a besoin d'être accessible 
à travers l'adresse IP de sortie du serveur sur lequel vous êtes connecté.

Exemple:
Si le port 1234 est relayé vers vous et que vous êtes
connecté sur le serveur se5.mullvad.net, alors vous 
serez joinable sur se5x.mullvad.net:1234

Les modifications ne prennent effet qu'après reconnexion. Mullvad Mullvad: connecté Mullvad: connexion Mullvad: déconnecté Pays-Bas Réseau Obfsproxy n'est pas installé.

Vous pouvez l'installer avec:
apt-get install obfsproxy
 Gestionnaire de port Port: %s Ports:  Protocole: %s Quitter Supprimer Envoyer L'envoi a échoué.

Détails:
 Serveur Paramètres Paramètres de Mullvad Fermeture en cours Singapour Statut Statut: %s Empêcher les fuites DNS Suède L'adaptateur de réseau virtuel TAP-Windows est manquant

Vous pouvez le réinstaller en re-exécutant le programme d'installation (Fermer le logiciel d'abord.) Votre compte a expiré.

Gérer votre compte? La période d'essai expire dans  Ces informations seront envoyées: Cette version de Mullvad ne supporte pas Windows XP. Veuillez mettre à jour votre système d'exploitation, ou utilisez une simple connexion OpenVPN. Temps restant: %s Pour faciliter la résolution du problème vous pouvez envoyer un rapport de débogage à mullvad.net. IPv6 États-Unis Impossible de créer un nouveau compte.
Veuillez vérifier votre connexion internet. Impossible de récuper une addresse Utiliser un compte existant Version Bienvenue sur Mullvad Écrivez une description du problème: Vous utilisez Mullvad à partir de l'image d'installation.
Ceci n'est pas pris en charge, vous devez d'abord copier le programme dans le dossier d'Applications et le lancer de là. Votre adresse e-mail: Votre connexion internet est maintenant
anonyme et cryptée par les serveurs
de Mullvad afin d'empêcher un tiers d'accéder
à vos communications. Votre nouveau numéro de compte: {} Votre fichier settings.ini est corrompu
Vos paramètres de configuration ont été sauvegardés ici:
 jour jours erreur %s récupération... heure heures aucun aucun [port] inconnu 